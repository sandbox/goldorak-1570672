#Purpose
Commerce_stock_status module aims to customize "out-of-stock" status for every
products on your website. Maybe you want to inform your customers when such
products will be available or you just want to set a default "out of stock"
status for a specified product type.

#Features
*[admin/commerce/config/stock/status] Provide an UI to enable stock_status field
 on the product type you want. You can define the default "out of stock" status
 for each product type.
*[admin/commerce/config/stock/update] Based on VBO, the batch processing tab
 allow you to update stock and stock_status field on 1...n products.

#Installation/Requirements
First, you have to install both commerce and commerce_cart modules.
* drush en commerce -y
* drush en commerce_cart -y
Download version 2 of commerce_stock (>=7.x-2.0-alpha1). Enable core 
module and also commerce_ss and commerce_ssr afterwards.
* drush en commerce_stock
* drush en commerce_ss
* drush en commerce_ssr
After theses installations, you can enable commerce_stock_status.
