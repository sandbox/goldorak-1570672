<?php

/**
 * @file
 * Views for updating stock attributes (commerce_stock & commerce_stock_status).
 */

/**
 * Implements hook_views_default_views().
 *
 * @return array
 *   Return an array containg the view.
 */
function commerce_stock_status_views_default_views() {
  $views             = array();
  $view              = new view();
  $view->name        = 'update_stock_attributes';
  $view->description = 'Update ';
  $view->tag         = 'default';
  $view->base_table  = 'commerce_product';
  $view->human_name  = 'Update Stock Attribute';
  $view->core        = 7;
  $view->api_version = '3.0';
  /* Edit this to true to make a default view disabled initially */
  $view->disabled = FALSE;

  /* Display: Master */

  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Update Stock Attributes';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '100';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = 0;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'product_id' => 'product_id',
    'sku' => 'sku',
    'title' => 'title',
    'commerce_stock' => 'commerce_stock',
    'commerce_stock_status' => 'commerce_stock_status',
    'views_bulk_operations' => 'views_bulk_operations',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'product_id' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'sku' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'commerce_stock' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'commerce_stock_status' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'views_bulk_operations' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 0;
  /* Field: Commerce Product: Product ID */

  $handler->display->display_options['fields']['product_id']['id'] = 'product_id';
  $handler->display->display_options['fields']['product_id']['table'] = 'commerce_product';
  $handler->display->display_options['fields']['product_id']['field'] = 'product_id';
  /* Field: Commerce Product: SKU */

  $handler->display->display_options['fields']['sku']['id'] = 'sku';
  $handler->display->display_options['fields']['sku']['table'] = 'commerce_product';
  $handler->display->display_options['fields']['sku']['field'] = 'sku';
  $handler->display->display_options['fields']['sku']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['sku']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['sku']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['sku']['alter']['external'] = 0;
  $handler->display->display_options['fields']['sku']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['sku']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['sku']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['sku']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['sku']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['sku']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['sku']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['sku']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['sku']['alter']['html'] = 0;
  $handler->display->display_options['fields']['sku']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['sku']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['sku']['hide_empty'] = 0;
  $handler->display->display_options['fields']['sku']['empty_zero'] = 0;
  $handler->display->display_options['fields']['sku']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['sku']['link_to_product'] = 0;
  /* Field: Commerce Product: Title */

  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'commerce_product';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['title']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['title']['link_to_product'] = 0;
  /* Field: Commerce Product: Stock */

  $handler->display->display_options['fields']['commerce_stock']['id'] = 'commerce_stock';
  $handler->display->display_options['fields']['commerce_stock']['table'] = 'field_data_commerce_stock';
  $handler->display->display_options['fields']['commerce_stock']['field'] = 'commerce_stock';
  $handler->display->display_options['fields']['commerce_stock']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['commerce_stock']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['commerce_stock']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['commerce_stock']['alter']['external'] = 0;
  $handler->display->display_options['fields']['commerce_stock']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['commerce_stock']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['commerce_stock']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['commerce_stock']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['commerce_stock']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['commerce_stock']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['commerce_stock']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['commerce_stock']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['commerce_stock']['alter']['html'] = 0;
  $handler->display->display_options['fields']['commerce_stock']['element_type'] = 'span';
  $handler->display->display_options['fields']['commerce_stock']['element_label_type'] = 'span';
  $handler->display->display_options['fields']['commerce_stock']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['commerce_stock']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['commerce_stock']['hide_empty'] = 0;
  $handler->display->display_options['fields']['commerce_stock']['empty_zero'] = 0;
  $handler->display->display_options['fields']['commerce_stock']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['commerce_stock']['settings'] = array(
    'thousand_separator' => ' ',
    'decimal_separator' => '.',
    'scale' => '2',
    'prefix_suffix' => 1,
  );
  $handler->display->display_options['fields']['commerce_stock']['field_api_classes'] = 0;
  /* Field: Commerce Product: Stock Status */

  $handler->display->display_options['fields']['commerce_stock_status']['id'] = 'commerce_stock_status';
  $handler->display->display_options['fields']['commerce_stock_status']['table'] = 'field_data_commerce_stock_status';
  $handler->display->display_options['fields']['commerce_stock_status']['field'] = 'commerce_stock_status';
  $handler->display->display_options['fields']['commerce_stock_status']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['commerce_stock_status']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['commerce_stock_status']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['commerce_stock_status']['alter']['external'] = 0;
  $handler->display->display_options['fields']['commerce_stock_status']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['commerce_stock_status']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['commerce_stock_status']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['commerce_stock_status']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['commerce_stock_status']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['commerce_stock_status']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['commerce_stock_status']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['commerce_stock_status']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['commerce_stock_status']['alter']['html'] = 0;
  $handler->display->display_options['fields']['commerce_stock_status']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['commerce_stock_status']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['commerce_stock_status']['hide_empty'] = 0;
  $handler->display->display_options['fields']['commerce_stock_status']['empty_zero'] = 0;
  $handler->display->display_options['fields']['commerce_stock_status']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['commerce_stock_status']['field_api_classes'] = 0;
  /* Field: Bulk operations: Commerce Product */

  $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['table'] = 'commerce_product';
  $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['label'] = 'Edit';
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['external'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['alter']['html'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['hide_empty'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['empty_zero'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo']['entity_load_capacity'] = '10';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo']['operations'] = array(
    'action::views_bulk_operations_delete_item' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::system_message_action' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_script_action' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_modify_action' => array(
      'selected' => 1,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'show_all_tokens' => 0,
        'display_values' => array(
          'commerce_stock' => 'commerce_stock',
          'commerce_stock_status' => 'commerce_stock_status',
        ),
      ),
    ),
    'action::views_bulk_operations_argument_selector_action' => array(
      'selected' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'url' => '',
      ),
    ),
    'action::system_goto_action' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::system_send_email_action' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
  );
  $handler->display->display_options['fields']['views_bulk_operations']['vbo']['enable_select_all_pages'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo']['display_type'] = '1';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo']['display_result'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo']['merge_single_action'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo']['force_single'] = 0;
  /* Filter criterion: Commerce Product: SKU */

  $handler->display->display_options['filters']['sku']['id'] = 'sku';
  $handler->display->display_options['filters']['sku']['table'] = 'commerce_product';
  $handler->display->display_options['filters']['sku']['field'] = 'sku';
  $handler->display->display_options['filters']['sku']['operator'] = 'contains';
  $handler->display->display_options['filters']['sku']['exposed'] = TRUE;
  $handler->display->display_options['filters']['sku']['expose']['operator_id'] = 'sku_op';
  $handler->display->display_options['filters']['sku']['expose']['label'] = 'SKU';
  $handler->display->display_options['filters']['sku']['expose']['operator'] = 'sku_op';
  $handler->display->display_options['filters']['sku']['expose']['identifier'] = 'sku';
  $handler->display->display_options['filters']['sku']['expose']['required'] = 0;
  $handler->display->display_options['filters']['sku']['expose']['multiple'] = FALSE;
  /* Filter criterion: Commerce Product: Stock (commerce_stock) */

  $handler->display->display_options['filters']['commerce_stock_value']['id'] = 'commerce_stock_value';
  $handler->display->display_options['filters']['commerce_stock_value']['table'] = 'field_data_commerce_stock';
  $handler->display->display_options['filters']['commerce_stock_value']['field'] = 'commerce_stock_value';
  $handler->display->display_options['filters']['commerce_stock_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['commerce_stock_value']['expose']['operator_id'] = 'commerce_stock_value_op';
  $handler->display->display_options['filters']['commerce_stock_value']['expose']['label'] = 'Stock';
  $handler->display->display_options['filters']['commerce_stock_value']['expose']['use_operator'] = 1;
  $handler->display->display_options['filters']['commerce_stock_value']['expose']['operator'] = 'commerce_stock_value_op';
  $handler->display->display_options['filters']['commerce_stock_value']['expose']['identifier'] = 'commerce_stock_value';
  $handler->display->display_options['filters']['commerce_stock_value']['expose']['multiple'] = FALSE;
  /* Filter criterion: Commerce Product: Stock Status (commerce_stock_status) */

  $handler->display->display_options['filters']['commerce_stock_status_value']['id'] = 'commerce_stock_status_value';
  $handler->display->display_options['filters']['commerce_stock_status_value']['table'] = 'field_data_commerce_stock_status';
  $handler->display->display_options['filters']['commerce_stock_status_value']['field'] = 'commerce_stock_status_value';
  $handler->display->display_options['filters']['commerce_stock_status_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['commerce_stock_status_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['commerce_stock_status_value']['expose']['operator_id'] = 'commerce_stock_status_value_op';
  $handler->display->display_options['filters']['commerce_stock_status_value']['expose']['label'] = 'Stock Status';
  $handler->display->display_options['filters']['commerce_stock_status_value']['expose']['operator'] = 'commerce_stock_status_value_op';
  $handler->display->display_options['filters']['commerce_stock_status_value']['expose']['identifier'] = 'commerce_stock_status_value';
  $handler->display->display_options['filters']['commerce_stock_status_value']['expose']['required'] = 0;
  $handler->display->display_options['filters']['commerce_stock_status_value']['expose']['multiple'] = FALSE;

  /* Display: Block */

  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['block_description'] = 'Update Stock Attributes';


  $views[$view->name] = $view;

  return $views;
}
