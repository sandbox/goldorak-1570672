<?php

/**
 * @file
 * Administrative callbacks and form builder functions
 * for Commerce Stock status.
 */

/**
 * Implements hook_form().
 *
 * Commerce Stock status admin form.
 *
 * @param array $form
 *   Form array.
 * @param array $form_status
 *   Form status array.
 *
 * @return array
 *   Return form array.
 */
function commerce_stock_status_admin_form($form, &$form_status) {
  // Find out what our status is. Use both the status of existing fields
  // and the status of variables to determine what's right.
  $field_name = 'commerce_stock_status';
  $field = field_info_field($field_name);

  // We will have many fields with the same name, so we need to be able to
  // access the form hierarchically.
  $form['#tree'] = TRUE;

  $form['product_types'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Enable and Edit Stock status for these product types'),
    '#description' => t("Note that disabling stock status will simply remove the relatedcd  field and you can't undo this action."),
    // '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  );

  // Create a checkbox for each product type and a textfield in order
  // to enter a default "out of stock" message.
  foreach (commerce_product_types() as $type => $product_type) {

    // Checking if the commerce_stock_status exists.
    $instance[$type] = field_info_instance('commerce_product', 'commerce_stock_status', $type);
    $enabled[$type] = (!empty($instance[$type]));

    // Form element :: checkbox.
    $form['product_types'][$type]['selected'] = array(
      '#type'          => 'checkbox',
      '#default_value' => $enabled[$type],
      '#title'         => t('@name (@machine_name)',
        array(
          '@name'         => $product_type['name'],
          '@machine_name' => $type,
        )),
    );

    // Form element :: checkbox.
    $form['product_types'][$type]['products'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Override all products stock status value with the new default value. (@product_type)',
        array('@product_type' => $type)),
      '#default_value' => FALSE,
    );

    // Form element :: textfield.
    $form['product_types'][$type]['status'] = array(
      '#type'          => 'textfield',
      '#attributes'    => array('placeholder' => t('Enter a status')),
      '#default_value' => $instance[$type]['default_value'][0]['value'],
      '#title'         => t('Default "Out of stock" status for @name',
        array('@name' => $product_type['name'])),
    );
  }
  // Add a checkbox that requires them to say "I do", but don't show it
  // (#access == FALSE) unless they're deleting.
  if (!empty($form_status['commerce_stock_status']['delete_instances'])) {
    $type_plural = format_plural(count($form_status['commerce_stock_status']['delete_instances']), t('type'), t('types'));
    $affirmation = t('I understand that all stock status data will be permanently removed from the product @type_plural %product_types.',
      array(
        '@type_plural'   => $type_plural,
        '%product_types' => implode(', ', $form_status['commerce_stock_status']['delete_instances']),
      )
    );
  }
  $form['confirmation'] = array(
    '#type'          => 'checkbox',
    '#title'         => check_plain(!empty($affirmation) ? $affirmation : ''),
    '#default_value' => FALSE,
    '#access'        => FALSE,
  );
  // Form element :: Submit button.
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Submit form'),
  );
  // If they're deleting, show the confirmation checkbox.
  if (!empty($form_status['commerce_stock_status']['delete_instances'])) {
    $form['confirmation']['#access'] = TRUE;
    drupal_set_message(t('You must click the confirmation checkbox to confirm that you want to delete stock status data'), 'warning');
  }

  return $form;
}

/**
 * Implements hook_form_validate().
 *
 * @param array $form
 *   Form array.
 * @param array $form_status
 *   Form status array.
 */
function commerce_stock_status_admin_form_validate($form, &$form_status) {
  if (!empty($form_status['commerce_stock_status']['delete_instances']) && empty($form_status['values']['confirmation'])) {
    form_set_error('confirmation',
      t('Please check the "I understand" checkbox to indicate you understand that all stock data in these fields will be deleted: %fields.',
        array('%fields' => implode(', ', $form_status['commerce_stock_status']['delete_instances']))
      )
    );
  }
}

/**
 * Implements hook_form_submit().
 *
 * @param array $form
 *   Form array.
 * @param array $form_status
 *   Form status array.
 */
function commerce_stock_status_admin_form_submit($form, &$form_status) {
  $form_status['commerce_stock_status']['delete_instances'] = array();
  foreach ($form_status['values']['product_types'] as $bundle => $values) {
    $instance = field_info_instance('commerce_product', 'commerce_stock_status', $bundle);

    // Checking if commerce stock status management is enabled.
    $currently_enabled = commerce_stock_status_product_type_enabled($bundle);

    // If they want us to enable it and it doesn't currently exist, do the work.
    // @todo => Check if stock is enabled in this product type ???
    if ($values['selected'] && !$currently_enabled) {
      commerce_stock_status_create_instance('commerce_stock_status', 'text', TRUE, 'commerce_product', $bundle, t('Stock Status'), $values['status']);
      commerce_stock_status_init_products($bundle, $values['status']);
      drupal_set_message(t('Stock management has been enabled on the %bundle product type', array('%bundle' => $bundle)));
    }
    // If bundle selected and stock status field enabled, go !!
    elseif ($values['selected'] && $currently_enabled) {

      // If the entered default stock status value is different than the old one
      // set the new value as default_value for the commerce_stock_status field.
      if ($instance['default_value'][0]['value'] != $values['status']) {
        commerce_stock_status_field_update_default_value($bundle, $values['status']);
        drupal_set_message(t('Default Stock status field has been updated on the %bundle product type', array('%bundle' => $bundle)));
      }

      // If $values['products'] === TRUE => apply entered stock status value
      // to all products of the specified bundle.
      if ($values['products']) {
        commerce_stock_status_init_products($bundle, $values['status']);
        drupal_set_message(t('(@bundle) Default Stock status of products have been updated to %value',
          array('@bundle' => $bundle, '%value' => $values['status'])
        ));
      }
    }
    // Conversely, if they *don't* want it and it's currently enabled,
    // warn them about the consequences or do it.
    elseif (!$values['selected'] && $currently_enabled) {
      // If they haven't clicked the "confirm" checkbox, rebuild and get them
      // to do it.
      if (empty($form_status['values']['confirmation'])) {
        $form_status['commerce_stock_status']['delete_instances'][] = $bundle;
        $form_status['rebuild'] = TRUE;
      }
      // Otherwise they already have clicked it and we can delete.
      else {
        // Remove the instance.
        field_delete_instance($instance);

        drupal_set_message(t('Stock status management has been disabled on the %bundle product type', array('%bundle' => $bundle)));
      }
    }
  }
}

/**
 * Showing the module default view.
 *
 * @return array
 *   Return a renderable array.
 */
function commerce_stock_status_admin_show_default_view() {
  $view = views_get_view('update_stock_attributes');
  if ($view->access('block')) {
    return $view->preview('block');
  }
}
