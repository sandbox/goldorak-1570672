<?php

/**
 * @file
 * Rules integration for Commerce Stock Status.
 */

/**
 * Implements hook_rules_action_info().
 *
 * @return array
 *   Return an array of actions for rules.
 */
function commerce_stock_status_rules_action_info() {
  $actions = array();

  $actions['commerce_stock_status_hide_field'] = array(
    'label' => t('Hide the stock status field.'),
    'group' => t('Commerce Stock Status'),
    'parameter' => array(
      'commerce_product' => array(
        'type' => 'commerce_product',
        'label' => t('Product'),
      ),
    ),
    'group' => t('Commerce Stock'),
    'callbacks' => array(
      'execute' => 'commerce_stock_status_rules_hide_field',
    ),
  );

  return $actions;
}

/**
 * Hide the stock status field.
 *
 * @param object $product
 *   A commerce product object
 */
function commerce_stock_status_rules_hide_field($product) {
  // @todo => Have to be improved !!!!
  unset($product->commerce_stock_status);
}
