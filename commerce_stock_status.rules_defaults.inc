<?php

/**
 * @file
 * Default rule configurations for Commerce Stock Status
 */


/**
 * Implements hook_default_rules_configuration().
 */
function commerce_stock_status_default_rules_configuration() {

  $rules = array();

  $rules_export = '{ "rules_stock_status_show_stock_status_field" : {
      "LABEL" : "Stock Status: Hide stock status field",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "commerce_ss", "rules", "commerce_stock_status", "commerce_stock" ],
      "ON" : [ "commerce_stock_check_add_to_cart_form_state" ],
      "IF" : [
        { "commerce_ss_stock_enabled_on_product" : { "commerce_product" : [ "commerce_product" ] } },
        { "commerce_ss_stock_not_disabled" : { "commerce_product" : [ "commerce_product" ] } },
        { "NOT data_is" : { "data" : [ "commerce-product:commerce-stock" ], "value" : "0" } }
      ],
      "DO" : [
        { "commerce_stock_status_hide_field" : { "commerce_product" : [ "commerce_product" ] } }
      ]
    }
  }';
  $rules['rules_hide_commerce_stock_status_field'] = rules_import($rules_export);
  return $rules;
}
